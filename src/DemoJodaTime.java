package com.wiedha.rizal.belajar.java;

import org.joda.time.DateTime;

public class DemoJodaTime {

	public static void main(String[] args) {
		DateTime sekarang = new DateTime();
		DateTime tigaBulanLagi = sekarang.plusMonths(4);

		System.out.println("Tiga Bulan dari sekarang :"+tigaBulanLagi);
	}
}